import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class MyDBHelperV1
{

	private Connection con = null;

<<<<<<< HEAD
	// copy for the exam
	public void init()
	{

		try
		{
			// 1a. Jar-Files referenzieren - Java Build Path
			// 1b. Load Access Driver

			Class.forName("net.ucanaccess.jdbc.UcanaccessDriver");
		} catch (ClassNotFoundException e)
		{

			e.printStackTrace();
		}

		try
		{

			// 2. get Connection to database
			con = DriverManager.getConnection("jdbc:ucanaccess://" + "C:\\Users\\denise.matschnig\\Desktop\\Denise\\Datenbankprogrammieren\\" + "Urlaubsverwaltung.accdb");

			System.out.println("Datenbank l�dt ....");
			System.out.println();

		} catch (SQLException e)
		{
			e.printStackTrace();
		}

	}

	public void close()
	{
		if (con != null)
			try
			{
				con.close();
			} catch (SQLException e)
			{
				e.printStackTrace();
				System.out.println("Fehler beim Schliessen der Verbindung");
			}
	}

	public void UrlaubHinzufuegen(String destination, double preis)
	{
		// insert into MeineUrlaube(Ziel, price) .....
	}

	public void insertUrlaub(Urlaub urlaub)
	{
		// insert into MeineUrlaube(Ziel, price) .....
	}

	public void UrlaubLoeschen(int urlaubsNr)
	{

	}

	public ArrayList<Urlaub> GetAlleUrlaubPreisGroesserAls(double preis)
	{

		return null;
	}

	public void printAlleUrlaube()
	{
		try
		{
			Statement stmt = con.createStatement();
			String query = "Select Ort, Preis FROM Urlaubswunsch";
			ResultSet rs = stmt.executeQuery(query);

			while (rs.next())
			{
				// System.out.println("%s, %s, %s%n", rs.getString(1), rs.getString(2),
				// rs.getString(3));
				String ort = rs.getString(1); // die Zahl am ende bezieht sich auf die Abfrage nicht auf die Tabelle
				double preis = rs.getDouble(2);

				System.out.printf("Ort: %s| Preis: %.2f \n", ort, preis);
			}
		} catch (SQLException e)
		{
			e.printStackTrace();
		}
	}

	public void printAllStudents()
	{
		try
		{
			Statement stmt = con.createStatement();
			String query = "Select Vorname, Nachname, Credits, Inskribiert, Lieblingsfarbe FROM Student";
			ResultSet rs = stmt.executeQuery(query);

			while (rs.next())
			{
				// System.out.println("%s, %s, %s%n", rs.getString(1), rs.getString(2),
				// rs.getString(3));
				String vorname = rs.getString(1); // die Zahl am ende bezieht sich auf die Abfrage nicht auf die Tabelle
				String nachname = rs.getString(2);
				double credits = rs.getDouble(3);
				boolean inskribiert = rs.getBoolean(4);
				String lieblingsfarbe = rs.getString(5);

				System.out.printf("Vorname: %s| Nachname: %s| Credits: %.2f| Inskribiert: %b| Lieblingsfarbe: %s \n",
						vorname, nachname, credits, inskribiert, lieblingsfarbe);
			}
		} catch (SQLException e)
		{
			e.printStackTrace();
		}
	}

	public void listKunden()
	{

		try
		{

			Statement stmt = con.createStatement();
			String sql = "SELECT * FROM Kunde ";

			ResultSet rs = stmt.executeQuery(sql);

			System.out.println("Meine Kunden");
			while (rs.next())
			{
				System.out.print("Kdnr: " + rs.getInt(1));
				System.out.println(" Vorname " + rs.getString(2));

			}
			rs.close();
			stmt.close();
		} catch (SQLException e)
		{
			e.printStackTrace();
		}

	}

	public void UrlaubeMitPreisGroesserAls(double Kosten)
	{
		try
		{
			Statement stmt = con.createStatement();
			String query = "Select Ort FROM Urlaubswunsch where Preis >" + Kosten + " ORDER BY Ort ";
			ResultSet rs = stmt.executeQuery(query);

			while (rs.next())
			{
				// System.out.println("%s, %s, %s%n", rs.getString(1), rs.getString(2),
				// rs.getString(3));
				String ort = rs.getString(1); // die Zahl am ende bezieht sich auf die Abfrage nicht auf die Tabelle

				System.out.printf("Ort: %s \n", ort);
			}
		} catch (SQLException e)
		{
			e.printStackTrace();
		}
	}

	public void gibDenStudentMitDerGesuchtenIdAus(int id)
	{
		try
		{
			Statement stmt = con.createStatement();
			String query = "Select Vorname, Nachname, Credits, Inskribiert, Lieblingsfarbe FROM Student Where StudentID = "
					+ id;
			ResultSet rs = stmt.executeQuery(query);

			if (rs.next() == true)
			{
				// System.out.println("%s, %s, %s%n", rs.getString(1), rs.getString(2),
				// rs.getString(3));
				String vorname = rs.getString(1); // die Zahl am ende bezieht sich auf die Abfrage nicht auf die Tabelle
				String nachname = rs.getString(2);
				double credits = rs.getDouble(3);
				boolean inskribiert = rs.getBoolean(4);
				String lieblingsfarbe = rs.getString(5);

				System.out.printf("Vorname: %s| Nachname: %s| Credits: %.2f| Inskribiert: %b| Lieblingsfarbe: %s \n",
						vorname, nachname, credits, inskribiert, lieblingsfarbe);
			} else
			{
				System.out.println("Diese ID ist unbekannt");
			}
		} catch (SQLException e)
		{
			e.printStackTrace();
		}
	}

	public void gibStudentenMitDerGesuchtenIDAus(int id)
	{
		String query = "Select Vorname, Nachname, Credits, Inskribiert, Lieblingsfarbe FROM Student "
				+ " Where StudentID = ? ";

		try
		{
			PreparedStatement stmt = con.prepareStatement(query);
			stmt.setInt(1, id);
			ResultSet rs = stmt.executeQuery();

			if (rs.next())
			{

				String vorname = rs.getString(1); // die Zahl am ende bezieht sich auf die Abfrage nicht auf die Tabelle
				String nachname = rs.getString(2);
				double credits = rs.getDouble(3);
				boolean inskribiert = rs.getBoolean(4);
				String lieblingsfarbe = rs.getString(5);

				System.out.printf("Vorname: %s| Nachname: %s| Credits: %.2f| Inskribiert: %b| Lieblingsfarbe: %s \n",
						vorname, nachname, credits, inskribiert, lieblingsfarbe);
			} else
			{
				System.out.println("Diese ID ist unbekannt");
			}
		} catch (SQLException e)
		{
			e.printStackTrace();
		}
	}

	public void printStudendAndUrlaube(int studentId, double preisGroesserAls)
	{
		String query = "SELECT Vorname FROM Student WHERE studentId=? ";
		try
		{
			PreparedStatement stmt = con.prepareStatement(query);
			stmt.setInt(1, studentId);
			ResultSet rs = stmt.executeQuery();
			if (rs.next())
			{
				String vorname = rs.getString(1);
				System.out.printf("Student: %s| ", vorname);

				String queryUrlaube = "SELECT preis, Ort from Urlaubswunsch where preis>? and studentId=?";

				PreparedStatement stmtFuerUrlaube = con.prepareStatement(queryUrlaube);
				stmtFuerUrlaube.setDouble(1, preisGroesserAls);
				stmtFuerUrlaube.setInt(2, studentId);
				ResultSet rsUrlaube = stmtFuerUrlaube.executeQuery();
				int counter = 0;
				while (rsUrlaube.next())
				{
					counter++;
					double preis = rsUrlaube.getDouble(1);
					String ort = rsUrlaube.getString(2);

					System.out.printf("Preis: %.2f| Ort: %s \n", preis, ort);
				}
				if (counter == 0)
				{
					System.out.println("Dieser Student hat keine Urlaubswuensche ");
				}
			} else
			{
				System.out.printf("Student: %d wurde nicht gefunden ", studentId);
			}
		} catch (SQLException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void gibAlleStudentenUndDerenUrlaubAus()
	{
		try
		{
			Statement stmt = con.createStatement();
			String query = "Select StudentID FROM Student";
			ResultSet rs = stmt.executeQuery(query);

			while (rs.next())
			{
				printStudendAndUrlaube(rs.getInt(1), 0);
			}
		} catch (SQLException e)
		{
			e.printStackTrace();
		}
	}

	public void printZusammenfassungProStudent()
	{
		String query = "SELECT Student.Vorname, avg(Urlaubswunsch.Preis), count(Urlaubswunsch.StudentID)\r\n"
				+ "		FROM Student, Urlaubswunsch WHERE Student.StudentID = Urlaubswunsch.StudentID\r\n"
				+ "		GROUP BY Vorname";
		try
		{
			PreparedStatement stmt = con.prepareStatement(query);
			ResultSet rs = stmt.executeQuery();

			while (rs.next())
			{
				String vorname = rs.getString(1);
				double preis = rs.getDouble(2);
				int urlaubswunsch = rs.getInt(3);

				System.out.printf("Vorname: %s| durchschnittlicher Preis: %.2f| Urlaubsw�nsche: %d \n", vorname, preis,
						urlaubswunsch);
			}
		} catch (SQLException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void printUrlaubsOrtStatistik()
	{

		String query = "SELECT Ort, COUNT(*)\r\n" + "		FROM Urlaubswunsch\r\n" + "GROUP BY Ort"
				+ " ORDER BY COUNT(*) desc ";
		try
		{
			PreparedStatement stmt = con.prepareStatement(query);
			ResultSet rs = stmt.executeQuery();

			while (rs.next())
			{
				String Ort = rs.getString(1);
				int countOrt = rs.getShort(2);

				System.out.printf("Ort: %s| Anzahl: %d \n", Ort, countOrt);
			}
		} catch (SQLException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public Student getStudent(int studentNr)
	{
		Student s = null;

		try
		{
			String query = "Select studentID, vorname, nachname, credits, inskribiert, lieblingsfarbe FROM Student"
					+ "WHERE StudentID = ?";
			PreparedStatement stmt = con.prepareStatement(query);
			stmt.setInt(1, studentNr);
			ResultSet rs = stmt.executeQuery(query);
			while (rs.next())
			{
				s = new Student(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getDouble(4), rs.getBoolean(5),
						rs.getString(6));

			}

		} catch (SQLException e)
		{
			e.printStackTrace();
		}
		return s;
	}
=======
	public void init()
		{

			try
			{
				// 1a. Jar-Files referenzieren - Java Build Path
				// 1b. Load Access Driver
				Class.forName("net.ucanaccess.jdbc.UcanaccessDriver");
			} catch (ClassNotFoundException e)
			{

				e.printStackTrace();
			}

			try
			{

				// 2. get Connection to database
				con = DriverManager.getConnection("jdbc:ucanaccess://"
						+ "C:/Users/jacqueline.ganser/Desktop/Jacy�s Ordner/Datenbankprogrammierung/"
						+ "Urlaubsverwaltung.accdb");

				System.out.println("Datenbank l�dt 3....");
				System.out.println("Datenbank l�dt 2....");
				System.out.println("Datenbank l�dt 1....");
				System.out.println();

			} catch (SQLException e)
			{
				e.printStackTrace();
			}

		}

	public void close()
		{
			if (con != null)
				try
				{
					con.close();
				} catch (SQLException e)
				{
					e.printStackTrace();
					System.out.println("Fehler beim Schliessen der Verbindung");
				}
		}

	public void UrlaubHinzufuegen(String destination, double preis)
		{
			// insert into MeineUrlaube(Ziel, price) .....
		}

	public void insertUrlaub(Urlaub urlaub)
		{
			// insert into MeineUrlaube(Ziel, price) .....
		}

	public void UrlaubLoeschen(int urlaubsNr)
		{

		}

	public ArrayList<Urlaub> GetAlleUrlaubPreisGroesserAls(double preis)
		{

			return null;
		}

	public void printAlleUrlaube()
		{
			try
			{
				Statement stmt = con.createStatement();
				String query = "Select Ort, Preis FROM Urlaubswunsch";
				ResultSet rs = stmt.executeQuery(query);

				while (rs.next())
				{
					// System.out.println("%s, %s, %s%n", rs.getString(1), rs.getString(2),
					// rs.getString(3));
					String ort = rs.getString(1); // die Zahl am ende bezieht sich auf die Abfrage nicht auf die Tabelle
					double preis = rs.getDouble(2);

					System.out.printf("Ort: %s| Preis: %.2f \n", ort, preis);
				}
			} catch (SQLException e)
			{
				e.printStackTrace();
			}
		}

	public void printAllStudents()
		{
			try
			{
				Statement stmt = con.createStatement();
				String query = "Select Vorname, Nachname, Credits, Inskribiert, Lieblingsfarbe FROM Student";
				ResultSet rs = stmt.executeQuery(query);

				while (rs.next())
				{
					// System.out.println("%s, %s, %s%n", rs.getString(1), rs.getString(2),
					// rs.getString(3));
					String vorname = rs.getString(1); // die Zahl am ende bezieht sich auf die Abfrage nicht auf die
														// Tabelle
					String nachname = rs.getString(2);
					double credits = rs.getDouble(3);
					boolean inskribiert = rs.getBoolean(4);
					String lieblingsfarbe = rs.getString(5);

					System.out.printf(
							"Vorname: %s| Nachname: %s| Credits: %.2f| Inskribiert: %b| Lieblingsfarbe: %s \n", vorname,
							nachname, credits, inskribiert, lieblingsfarbe);
				}
			} catch (SQLException e)
			{
				e.printStackTrace();
			}
		}

	public void listKunden()
		{

			try
			{

				Statement stmt = con.createStatement();
				String sql = "SELECT * FROM Kunde ";

				ResultSet rs = stmt.executeQuery(sql);

				System.out.println("Meine Kunden");
				while (rs.next())
				{
					System.out.print("Kdnr: " + rs.getInt(1));
					System.out.println(" Vorname " + rs.getString(2));

				}
				rs.close();
				stmt.close();
			} catch (SQLException e)
			{
				e.printStackTrace();
			}

		}

	public void UrlaubeMitPreisGroesserAls(double Kosten)
		{
			try
			{
				Statement stmt = con.createStatement();
				String query = "Select Ort FROM Urlaubswunsch where Preis >" + Kosten + " ORDER BY Ort ";
				ResultSet rs = stmt.executeQuery(query);

				while (rs.next())
				{
					// System.out.println("%s, %s, %s%n", rs.getString(1), rs.getString(2),
					// rs.getString(3));
					String ort = rs.getString(1); // die Zahl am ende bezieht sich auf die Abfrage nicht auf die Tabelle

					System.out.printf("Ort: %s \n", ort);
				}
			} catch (SQLException e)
			{
				e.printStackTrace();
			}
		}

	public void gibDenStudentMitDerGesuchtenIdAus(int id)
		{
			try
			{
				Statement stmt = con.createStatement();
				String query = "Select Vorname, Nachname, Credits, Inskribiert, Lieblingsfarbe FROM Student Where StudentID = "
						+ id;
				ResultSet rs = stmt.executeQuery(query);

				if (rs.next() == true)
				{
					// System.out.println("%s, %s, %s%n", rs.getString(1), rs.getString(2),
					// rs.getString(3));
					String vorname = rs.getString(1); // die Zahl am ende bezieht sich auf die Abfrage nicht auf die
														// Tabelle
					String nachname = rs.getString(2);
					double credits = rs.getDouble(3);
					boolean inskribiert = rs.getBoolean(4);
					String lieblingsfarbe = rs.getString(5);

					System.out.printf(
							"Vorname: %s| Nachname: %s| Credits: %.2f| Inskribiert: %b| Lieblingsfarbe: %s \n", vorname,
							nachname, credits, inskribiert, lieblingsfarbe);
				} else
				{
					System.out.println("Diese ID ist unbekannt");
				}
			} catch (SQLException e)
			{
				e.printStackTrace();
			}
		}

	public void gibStudentenMitDerGesuchtenIDAus(int id)
		{
			String query = "Select Vorname, Nachname, Credits, Inskribiert, Lieblingsfarbe FROM Student "
					+ " Where StudentID = ? ";

			try
			{
				PreparedStatement stmt = con.prepareStatement(query);
				stmt.setInt(1, id);
				ResultSet rs = stmt.executeQuery();

				if (rs.next())
				{

					String vorname = rs.getString(1); // die Zahl am ende bezieht sich auf die Abfrage nicht auf die
														// Tabelle
					String nachname = rs.getString(2);
					double credits = rs.getDouble(3);
					boolean inskribiert = rs.getBoolean(4);
					String lieblingsfarbe = rs.getString(5);

					System.out.printf(
							"Vorname: %s| Nachname: %s| Credits: %.2f| Inskribiert: %b| Lieblingsfarbe: %s \n", vorname,
							nachname, credits, inskribiert, lieblingsfarbe);
				} else
				{
					System.out.println("Diese ID ist unbekannt");
				}
			} catch (SQLException e)
			{
				e.printStackTrace();
			}
		}

	public void printStudendAndUrlaube(int studentId, double preisGroesserAls)
		{
			String query = "SELECT Vorname FROM Student WHERE studentId=? ";
			try
			{
				PreparedStatement stmt = con.prepareStatement(query);
				stmt.setInt(1, studentId);
				ResultSet rs = stmt.executeQuery();
				if (rs.next())
				{
					String vorname = rs.getString(1);
					System.out.printf("Student: %s| ", vorname);

					String queryUrlaube = "SELECT preis, Ort from Urlaubswunsch where preis>? and studentId=?";

					PreparedStatement stmtFuerUrlaube = con.prepareStatement(queryUrlaube);
					stmtFuerUrlaube.setDouble(1, preisGroesserAls);
					stmtFuerUrlaube.setInt(2, studentId);
					ResultSet rsUrlaube = stmtFuerUrlaube.executeQuery();
					int counter = 0;
					while (rsUrlaube.next())
					{
						counter++;
						double preis = rsUrlaube.getDouble(1);
						String ort = rsUrlaube.getString(2);

						System.out.printf("Preis: %.2f| Ort: %s \n", preis, ort);
					}
					if (counter == 0)
					{
						System.out.println("Dieser Student hat keine Urlaubswuensche ");
					}
				} else
				{
					System.out.printf("Student: %d wurde nicht gefunden ", studentId);
				}
			} catch (SQLException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
>>>>>>> branch 'master' of https://denisematschnig@bitbucket.org/denisematschnig/datenbanken.git
}
