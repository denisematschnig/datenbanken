
public class Student
{
	private int		StudentNr;
	private String	Vorname;
	private String	Nachname;
	private double	Credits;
	private boolean	Inskribiert;
	private String	Lieblingsfarbe;

	public Student(int studentNr, String vorname, String nachname, double credits, boolean inskribiert,
			String lieblingsfarbe)
	{
		super();
		StudentNr = studentNr;
		Vorname = vorname;
		Nachname = nachname;
		Credits = credits;
		Inskribiert = inskribiert;
		Lieblingsfarbe = lieblingsfarbe;
	}

	@Override
	public String toString()
	{
		return "Student [StudentNr=" + StudentNr + ", Vorname=" + Vorname + ", Nachname=" + Nachname + ", Credits="
				+ Credits + ", Inskribiert=" + Inskribiert + ", Lieblingsfarbe=" + Lieblingsfarbe + "]";
	}

}
