
public class Rechnungen
{

	private int ReNr;

	public int getReNr()
		{
			return ReNr;
		}

	public void setReNr(int reNr)
		{
			ReNr = reNr;
		}

	public String getDatum()
		{
			return Datum;
		}

	public void setDatum(String datum)
		{
			Datum = datum;
		}

	public double getGesamtbetrag()
		{
			return Gesamtbetrag;
		}

	public void setGesamtbetrag(double gesamtbetrag)
		{
			Gesamtbetrag = gesamtbetrag;
		}

	public int getKDNR()
		{
			return KDNR;
		}

	public void setKDNR(int kDNR)
		{
			KDNR = kDNR;
		}

	private String Datum;
	private double Gesamtbetrag;
	private int KDNR;
}
