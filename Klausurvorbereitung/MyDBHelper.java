import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MyDBHelper
{

	private Connection con = null;

	public void init()
		{

			try
			{
				// 1a. Jar-Files referenzieren - Java Build Path
				// 1b. Load Access Driver
				Class.forName("net.ucanaccess.jdbc.UcanaccessDriver");
			} catch (ClassNotFoundException e)
			{

				e.printStackTrace();
			}

			try
			{

				// 2. get Connection to database
				con = DriverManager.getConnection(
						"jdbc:ucanaccess://" + "E:\\Datenbanken\\Neuer Ordner\\DBPrjKlausurvorbereitung.accdb");

				System.out.println("Aufgabe 3 - SUPER - hat funktioniert :-) ");

			} catch (SQLException e)
			{
				e.printStackTrace();
			}

		}

	public void close()
		{
			if (con != null)
				try
				{
					con.close();
				} catch (SQLException e)
				{
					e.printStackTrace();
					System.out.println("Fehler beim Schliessen der Verbindung");
				}
		}

	// Aufgabe 4
	public Kunde getKunde(int kdnr)
		{
			String query = "SELECT KDNR, Vorname, Nachname, Geschlecht, Bonuspunkte " + "FROM Kunden WHERE kdnr=? ";
			Kunde foundKunde = new Kunde();
			try
			{
				PreparedStatement stmt = con.prepareStatement(query);
				stmt.setInt(1, kdnr);
				ResultSet rs = stmt.executeQuery();
				if (rs.next())
				{
					int id = rs.getInt(1);
					foundKunde.setKDNR(id);
					foundKunde.setVorname(rs.getString(2));
					foundKunde.setNachname(rs.getString(3));
					foundKunde.setGeschlecht(rs.getString(4));
					foundKunde.setBonsupunkte(rs.getDouble(5));

				}
			} catch (SQLException e)
			{
				e.printStackTrace();
			}

			return foundKunde;
		}

	public List<Kunde> getAlleKunden()
		{
			ArrayList<Kunde> allK = new ArrayList<Kunde>();
			String query = "SELECT KDNR FROM Kunden";

			try
			{
				PreparedStatement stmt = con.prepareStatement(query);

				ResultSet rs = stmt.executeQuery();
				while (rs.next())
				{
					allK.add(getKunde(rs.getInt(1)));
				}
			} catch (SQLException e)
			{
				e.printStackTrace();
			}

			return allK;

		}

	public int insertKunde(Kunde newK)
		{
			int affectedRows = 0;
			// 1a - update Statement
			String insertString = "";
			insertString = "INSERT INTO Kunden(Vorname, Nachname, Geschlecht, Bonuspunkte) ";
			insertString += " VALUES(?,?,?,?) ";

			int newID = 0;
			try
			{

				con.setAutoCommit(true);

				PreparedStatement stmtInsert = con.prepareStatement(insertString);

				stmtInsert.setString(1, newK.getVorname());
				stmtInsert.setString(2, newK.getNachname());
				stmtInsert.setString(3, newK.getGeschlecht());
				stmtInsert.setDouble(4, newK.getBonsupunkte());

				affectedRows = stmtInsert.executeUpdate();

				String newIDS = "SELECT @@identity ";

				Statement stmt = con.createStatement();
				ResultSet rs = stmt.executeQuery(newIDS);

				rs.next();

				newID = rs.getInt(1);

			} catch (SQLException e)
			{
				//
				e.printStackTrace();
			}
			return newID;
		}

	public int insertRechnung(Rechnungen neueRechnung, Kunde bestehenderKunde)
		{
			int affectedRows = 0;

			String insertString = "";
			insertString = "INSERT INTO Rechnungen(Gesamtbetrag, Datum, KDNR) ";
			insertString += " VALUES(?,?,?) ";

			int newIdentity = 0;
			try
			{

				con.setAutoCommit(true);
				//
				PreparedStatement stmtInsert = con.prepareStatement(insertString);

				stmtInsert.setDouble(1, neueRechnung.getGesamtbetrag());
				stmtInsert.setString(2, neueRechnung.getDatum());

				stmtInsert.setInt(3, bestehenderKunde.getKDNR());

				affectedRows = stmtInsert.executeUpdate();

				String newIdentityString = "SELECT @@identity ";

				Statement stmt = con.createStatement();
				ResultSet rs = stmt.executeQuery(newIdentityString);

				rs.next();

				newIdentity = rs.getInt(1);

			} catch (SQLException e)
			{
				//
				e.printStackTrace();
			}
			return newIdentity;
		}

	public void updateKunde(Kunde k)
		{

			int affectedRows = 0;

			String update = "";
			update = "UPDATE Kunden SET ";
			update += " Vorname = ?, ";
			update += " Nachname = ?, ";
			update += " Geschlecht = ?, ";
			update += " Bonuspunkte = ? ";
			update += " WHERE KDNR=?";

			try
			{

				con.setAutoCommit(true);
				// 1b Prepared Statement
				PreparedStatement stmtUpd = con.prepareStatement(update);

				stmtUpd.setString(1, k.getVorname());
				stmtUpd.setString(2, k.getNachname());
				stmtUpd.setString(3, k.getGeschlecht());
				stmtUpd.setDouble(4, k.getBonsupunkte());
				stmtUpd.setInt(5, k.getKDNR());

				affectedRows = stmtUpd.executeUpdate();

			} catch (SQLException e)
			{
				//
				e.printStackTrace();
			}

		}

	public void updateRechnung(Rechnungen r)
		{

			int affectedRows = 0;

			String updateString = "";
			updateString = "UPDATE Rechnungen SET ";
			updateString += " Gesamtbetrag=?, ";
			updateString += " Datum=?, ";
			updateString += " KDNR=? ";
			updateString += " WHERE ReNr=?";

			try
			{

				con.setAutoCommit(true);
				// 1b Prepared Statement
				PreparedStatement stmtUpdate = con.prepareStatement(updateString);

				stmtUpdate.setDouble(1, r.getGesamtbetrag());
				stmtUpdate.setString(2, r.getDatum());
				stmtUpdate.setInt(3, r.getKDNR());
				stmtUpdate.setInt(4, r.getReNr());

				affectedRows = stmtUpdate.executeUpdate();

			} catch (SQLException e)
			{
				//
				e.printStackTrace();
			}

		}

	public void demoTransaktion()
		{

			String update1 = "UPDATE Kunden set bonuspunkte = bonuspunkte + 10 where kdnr=1";
			String update2 = "UPDATE Kunden set bonuspunkte = bonuspunkte - 10 where kdnr=3";
			try
			{

				// verhindert automatisches Speichern nach JEDEM executeUpdate
				con.setAutoCommit(false);
				PreparedStatement stmtUpd1 = con.prepareStatement(update1);
				int affected1 = stmtUpd1.executeUpdate();

				PreparedStatement stmtUpd2 = con.prepareStatement(update2);
				int affected2 = stmtUpd2.executeUpdate();

				//
				if (affected1 == 1 && affected2 == 1)
					con.commit();
				// Dauerhaftes Speichern wenn sowohl Statment1 als auch Statment2 jeweils eine
				// Zeile betroffen haben

				if (affected1 == 0 || affected2 == 0)
					con.rollback(); // alles R�ckg�ngig machen

			} catch (SQLException e)
			{
				//
				e.printStackTrace();

			}
		}

}
