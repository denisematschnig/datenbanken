import java.util.ArrayList;

public class Studenten
{
	private int						StudID;
	private String					Vorname;
	private String					Nachname;
	private int						Lieblingsgenre;
	private int						Bonuspunkte;
	private ArrayList<Musikgenre>	favGenre;

	public int getStudID()
	{
		return StudID;
	}

	public void setStudID(int studID)
	{
		StudID = studID;
	}

	public String getVorname()
	{
		return Vorname;
	}

	public void setVorname(String vorname)
	{
		Vorname = vorname;
	}

	public String getNachname()
	{
		return Nachname;
	}

	public void setNachname(String nachname)
	{
		Nachname = nachname;
	}

	public int getLieblingsgenre()
	{
		return Lieblingsgenre;
	}

	public void setLieblingsgenre(int lieblingsgenre)
	{
		Lieblingsgenre = lieblingsgenre;
	}

	public int getBonuspunkte()
	{
		return Bonuspunkte;
	}

	public void setBonuspunkte(int bonuspunkte)
	{
		Bonuspunkte = bonuspunkte;
	}

	public ArrayList<Musikgenre> getGenre()
	{
		return favGenre;
	}

	public void setGenre(ArrayList<Musikgenre> fave)
	{
		favGenre = fave;
	}

	@Override
	public String toString()
	{
		return String.format("(%d - %s - %s - %d - %d)\n", StudID, Vorname, Nachname, Lieblingsgenre, Bonuspunkte);

	}

}
