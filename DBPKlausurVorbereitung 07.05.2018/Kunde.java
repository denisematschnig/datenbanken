
public class Kunde
{

	@Override
	public String toString()
	{
		return "Kunde [KDNR=" + KDNR + ", Vorname=" + Vorname + ", Nachname=" + Nachname + ", Geschlecht=" + Geschlecht
				+ ", Bonsupunkte=" + Bonsupunkte + "]";
	}

	private int KDNR;

	public int getKDNR()
	{
		return KDNR;
	}

	public void setKDNR(int kDNR)
	{
		KDNR = kDNR;
	}

	public String getVorname()
	{
		return Vorname;
	}

	public void setVorname(String vorname)
	{
		Vorname = vorname;
	}

	public String getNachname()
	{
		return Nachname;
	}

	public void setNachname(String nachname)
	{
		Nachname = nachname;
	}

	public String getGeschlecht()
	{
		return Geschlecht;
	}

	public void setGeschlecht(String geschlecht)
	{
		Geschlecht = geschlecht;
	}

	public double getBonsupunkte()
	{
		return Bonsupunkte;
	}

	public void setBonsupunkte(double bonsupunkte)
	{
		Bonsupunkte = bonsupunkte;
	}

	private String	Vorname;
	private String	Nachname;
	private String	Geschlecht;
	private double	Bonsupunkte;
}
