import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MyDBHelper
{

	private Connection con = null;

	public void init()
	{

		try
		{
			// 1a. Jar-Files referenzieren - Java Build Path
			// 1b. Load Access Driver
			Class.forName("net.ucanaccess.jdbc.UcanaccessDriver");
		} catch (ClassNotFoundException e)
		{

			e.printStackTrace();
		}

		try
		{

			// 2. get Connection to database
			con = DriverManager
					.getConnection("jdbc:ucanaccess://" + "C:\\temp\\DBPSS2018\\DBPrjKlausurvorbereitung.accdb");

			System.out.println("Aufgabe 3 - SUPER - hat funktioniert :-) ");

		} catch (SQLException e)
		{
			e.printStackTrace();
		}

	}

	public void close()
	{
		if (con != null)
			try
			{
				con.close();
			} catch (SQLException e)
			{
				e.printStackTrace();
				System.out.println("Fehler beim Schliessen der Verbindung");
			}
	}

	// Aufgabe 4
	public Kunde getKunde(int kdnr)
	{
		String query = "SELECT KDNR, Vorname, Nachname, Geschlecht, Bonuspunkte ";
		query += " FROM Kunden WHERE kdnr=? ";
		Kunde foundKunde = new Kunde();
		try
		{
			PreparedStatement stmt = con.prepareStatement(query);
			stmt.setInt(1, kdnr);
			ResultSet rs = stmt.executeQuery();
			if (rs.next())
			{
				int id = rs.getInt(1);
				foundKunde.setKDNR(id);
				foundKunde.setVorname(rs.getString(2));
				foundKunde.setNachname(rs.getString(3));
				foundKunde.setGeschlecht(rs.getString(4));
				foundKunde.setBonsupunkte(rs.getDouble(5));

			}
		} catch (SQLException e)
		{
			e.printStackTrace();
		}

		return foundKunde;
	}

	public List<Kunde> getAlleKunden()
	{
		ArrayList<Kunde> alleKunden = new ArrayList<Kunde>();
		String query = "SELECT KDNR FROM Kunden";

		try
		{
			PreparedStatement stmt = con.prepareStatement(query);

			ResultSet rs = stmt.executeQuery();
			while (rs.next())
			{
				alleKunden.add(getKunde(rs.getInt(1)));
			}
		} catch (SQLException e)
		{
			e.printStackTrace();
		}

		return alleKunden;

	}

	public int insertKunde(Kunde neuerKunde)
	{
		int affectedRows = 0;
		// 1a - update Statement
		String insertString = "";
		insertString = "INSERT INTO Kunden(Vorname, Nachname, Geschlecht, Bonuspunkte) ";
		insertString += " VALUES(?,?,?,?) ";

		int newIdentity = 0;
		try
		{

			con.setAutoCommit(true);
			//
			PreparedStatement stmtInsert = con.prepareStatement(insertString);

			stmtInsert.setString(1, neuerKunde.getVorname());
			stmtInsert.setString(2, neuerKunde.getNachname());
			stmtInsert.setString(3, neuerKunde.getGeschlecht());
			stmtInsert.setDouble(4, neuerKunde.getBonsupunkte());

			affectedRows = stmtInsert.executeUpdate();

			String newIdentityString = "SELECT @@identity ";

			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery(newIdentityString);

			rs.next();

			newIdentity = rs.getInt(1);

		} catch (SQLException e)
		{
			//
			e.printStackTrace();
		}
		return newIdentity;
	}

	public int insertRechnung(Rechnungen neueRechnung, Kunde bestehenderKunde)
	{
		int affectedRows = 0;

		String insertString = "";
		insertString = "INSERT INTO Rechnungen(Gesamtbetrag, Datum, KDNR) ";
		insertString += " VALUES(?,?,?) ";

		int newIdentity = 0;
		try
		{

			con.setAutoCommit(true);
			//
			PreparedStatement stmtInsert = con.prepareStatement(insertString);

			stmtInsert.setDouble(1, neueRechnung.getGesamtbetrag());
			stmtInsert.setString(2, neueRechnung.getDatum());

			stmtInsert.setInt(3, bestehenderKunde.getKDNR());

			affectedRows = stmtInsert.executeUpdate();

			String newIdentityString = "SELECT @@identity ";

			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery(newIdentityString);

			rs.next();

			newIdentity = rs.getInt(1);
		} 
		
		catch (SQLException e)
		{
			//
			e.printStackTrace();
		}
		return newIdentity;
	}

	public void updateKunde(Kunde k)
	{
		int affectedRows = 0;

		String updateString = "";
		updateString = "UPDATE Kunden SET ";
		updateString += " Vorname = ?, ";
		updateString += " Nachname = ?, ";
		updateString += " Geschlecht = ?, ";
		updateString += " Bonuspunkte = ? ";
		updateString += " WHERE KDNR=?";

		try
		{
			con.setAutoCommit(true);
			// 1b Prepared Statement
			PreparedStatement stmtUpdate = con.prepareStatement(updateString);

			stmtUpdate.setString(1, k.getVorname());
			stmtUpdate.setString(2, k.getNachname());
			stmtUpdate.setString(3, k.getGeschlecht());
			stmtUpdate.setDouble(4, k.getBonsupunkte());
			stmtUpdate.setInt(5, k.getKDNR());

			affectedRows = stmtUpdate.executeUpdate();
		} 
		
		catch (SQLException e)
		{
			//
			e.printStackTrace();
		}

	}

	public void updateRechnung(Rechnungen r)
	{
		int affectedRows = 0;

		String updateString = "";
		updateString = "UPDATE Rechnungen SET ";
		updateString += " Gesamtbetrag=?, ";
		updateString += " Datum=?, ";
		updateString += " KDNR=? ";
		updateString += " WHERE ReNr=?";

		try
		{
			con.setAutoCommit(true);
			// 1b Prepared Statement
			PreparedStatement stmtUpdate = con.prepareStatement(updateString);

			stmtUpdate.setDouble(1, r.getGesamtbetrag());
			stmtUpdate.setString(2, r.getDatum());
			stmtUpdate.setInt(3, r.getKDNR());
			stmtUpdate.setInt(4, r.getReNr());

			affectedRows = stmtUpdate.executeUpdate();
		} 
		
		catch (SQLException e)
		{
			//
			e.printStackTrace();
		}
	}

	public void demoTransaktion()
	{
		String updateString1 = "UPDATE Kunden set bonuspunkte = bonuspunkte + 10 where kdnr=1";
		String updateString2 = "UPDATE Kunden set bonuspunkte = bonuspunkte - 10 where kdnr=3";
		try
		{
			// verhindert automatisches Speichern nach JEDEM executeUpdate
			con.setAutoCommit(false);
			PreparedStatement stmtUpdate1 = con.prepareStatement(updateString1);
			int affected1 = stmtUpdate1.executeUpdate();

			PreparedStatement stmtUpdate2 = con.prepareStatement(updateString2);
			int affected2 = stmtUpdate2.executeUpdate();

			//
			if (affected1 == 1 && affected2 == 1)
				con.commit();
			// Dauerhaftes Speichern wenn sowohl Statment1 als auch Statment2 jeweils eine
			// Zeile betroffen haben

			if (affected1 == 0 || affected2 == 0)
				con.rollback(); // alles R�ckg�ngig machen
		} 
		
		catch (SQLException e)
		{
			//
			e.printStackTrace();
		}
	}

	public List<Rechnungen> getRechnungenByKunde(int kdnr)
	{
		ArrayList<Rechnungen> rechnungen = new ArrayList<Rechnungen>();
		String query = "SELECT ReNr, Datum, Gesamtbetrag FROM Rechnungen WHERE Kdnr=?";

		try
		{
			PreparedStatement stmt = con.prepareStatement(query);
			stmt.setInt(1, kdnr);

			ResultSet rs = stmt.executeQuery();
			while (rs.next())
			{
				Rechnungen r = new Rechnungen();
				r.setReNr(rs.getInt(1));
				r.setDatum(rs.getString(2));
				r.setGesamtbetrag(rs.getDouble(3));
				r.setKDNR(kdnr);
				rechnungen.add(r);
			}
		} 
				
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		return rechnungen;
	}

	public ArrayList<Kunde> getWeiblicheKunden()
	{
		ArrayList<Kunde> weiblicheKunden = new ArrayList<Kunde>();
		String query = "SELECT KDNR FROM Kunden WHERE Geschlecht='W'";

		try
		{
			PreparedStatement stmt = con.prepareStatement(query);

			ResultSet rs = stmt.executeQuery();
			while (rs.next())
			{
				weiblicheKunden.add(getKunde(rs.getInt(1)));
			}
		} catch (SQLException e)
		{
			e.printStackTrace();
		}

		return weiblicheKunden;
	}

	public Kunde getKundeMitDenMeistenBonusPunkten()
	{
		Kunde kundeMitMeistenBonuspunkten = new Kunde();

		String query = "SELECT TOP 1 KDNR FROM Kunden order by bonuspunkte desc";
		// SELECT * FROM Kunden WHERE bonuspunkte = (SELECT MAX(Bonuspunkte)FROM KUNDEN)
		try
		{
			PreparedStatement stmt = con.prepareStatement(query);

			ResultSet rs = stmt.executeQuery();
			if (rs.next())
			{
				kundeMitMeistenBonuspunkten = getKunde(rs.getInt(1));
			}
		} 
		
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		return kundeMitMeistenBonuspunkten;
	}

	public void loescheAlleRechnungenUndDanachDenKunden(Kunde kDelete)
	{
		String deleteString1 = "DELETE * FROM Rechnungen WHERE KDNR=?";
		String deleteString2 = "DELETE * FROM Kunden WHERE kdnr=?";

		try
		{
			// verhindert automatisches Speichern nach JEDEM executeUpdate
			con.setAutoCommit(false);
			PreparedStatement stmtDelete1 = con.prepareStatement(deleteString1);
			stmtDelete1.setInt(1, kDelete.getKDNR());
			int affected1 = stmtDelete1.executeUpdate();

			PreparedStatement stmtDelete2 = con.prepareStatement(deleteString2);
			stmtDelete2.setInt(1, kDelete.getKDNR());
			int affected2 = stmtDelete2.executeUpdate();

			con.commit();

		}

		catch (SQLException e)
		{

			e.printStackTrace();
		}
	}

}
