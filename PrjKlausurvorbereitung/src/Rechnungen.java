
public class Rechnungen
{
	private int		RENR;
	private String	Datum;
	private double	Gesamtbetrag;
	private int		KDNR;

	public int getRENR()
	{
		return RENR;
	}

	public void setRENR(int rENR)
	{
		RENR = rENR;
	}

	public String getDatum()
	{
		return Datum;
	}

	public void setDatum(String datum)
	{
		Datum = datum;
	}


	public double getGesamtbetrag()
	{
		return Gesamtbetrag;
	}

	public void setGesamtbetrag(double gesamtbetrag)
	{
		Gesamtbetrag = gesamtbetrag;
	}

	public int getKDNR()
	{
		return KDNR;
	}

	public void setKDNR(int kDNR)
	{
		KDNR = kDNR;
	}

	@Override
	public String toString()
	{
		return "Rechnungen [RENR=" + RENR + ", Datum=" + Datum + ", Gesamtbetrag=" + Gesamtbetrag + ", KDNR=" + KDNR
				+ "]";
	}
}
