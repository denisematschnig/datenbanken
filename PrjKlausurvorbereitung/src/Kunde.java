
public class Kunde
{
	private int		KDNR;
	private String	Vorname;
	private String	Nachname;
	private String	Geschlecht;
	private int		Bonuspunkte;

	public Kunde()
	{

	}
	public int getKDNR()
		{
			return KDNR;
		}

	public String getVorname()
	{
		return Vorname;
	}


	public String getNachname()
		{
			return Nachname;
		}
	
	public String getGeschlecht()
		{
			return Geschlecht;
		}

	public int getBonuspunkte()
		{
			return Bonuspunkte;
		}

	public void setKDNR(int kDNR)
	{
		KDNR = kDNR;
	}

	public void setVorname(String vorname)
	{
		Vorname = vorname;
	}


	public void setNachname(String nachname)
	{
		Nachname = nachname;
	}


	public void setGeschlecht(String geschlecht)
	{
		Geschlecht = geschlecht;
	}


	public void setBonuspunkte(int bonuspunkte)
	{
		Bonuspunkte = bonuspunkte;
	}

	@Override
	public String toString()
	{
		return "-KdNr= " + KDNR + ", Vorname= " + Vorname + ", Nachname= " + Nachname + ", Geschlecht= " + Geschlecht
				+ ", Bonuspunkte= " + Bonuspunkte + "-\n";
	}
}
