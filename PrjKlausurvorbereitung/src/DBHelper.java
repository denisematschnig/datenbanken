import java.util.*;
import java.sql.*;

public class DBHelper
{
	// Issue 3
	private Connection con = null;

	public void init()
	{
		try
		{
			Class.forName("net.ucanaccess.jdbc.UcanaccessDriver");
		}

		catch (ClassNotFoundException e)
		{
			e.printStackTrace();
		}

		try
		{
			con = DriverManager
					.getConnection("jdbc:ucanaccess://" + "E:\\Datenbanken\\Übungsklausur\\" + "Übungsklausur.accdb");

			System.out.println("Super - hat funktioniert :-) ");

		} catch (SQLException e)
		{
			e.printStackTrace();
		}

	}

	public void close()
	{
		if (con != null)
			try
			{
				con.close();
			} catch (SQLException e)
			{
				e.printStackTrace();
				System.out.println("Fehler beim Schliessen der Verbindung");
			}
	}

	// Issue 4
	public Kunde getKunde(int kdnr)
	{
		String query = "Select KDNR, Vorname, Nachname, Geschlecht, Bonuspunkte FROM Kunden WHERE KDNR = ?";
		Kunde found = new Kunde();

		try
		{
			PreparedStatement stmt = con.prepareStatement(query);
			stmt.setInt(1, kdnr);
			ResultSet rs = stmt.executeQuery();

			if (rs.next())
			{
				int id = rs.getInt(1);
				found.setKDNR(kdnr);
				String vorname = rs.getString(2);
				found.setVorname(vorname);
				found.setNachname(rs.getString(3));
				found.setGeschlecht(rs.getString(4));
				found.setBonuspunkte(rs.getInt(5));
			}
		}

		catch (SQLException e)
		{
			e.printStackTrace();
		}

		return found;
	}

	// Issue 4a
	public List<Kunde> getAlleKunden()
	{
		List<Kunde> all = new ArrayList<Kunde>();
		String query = "Select KDNR, Vorname, Nachname, Geschlecht, Bonuspunkte FROM Kunden";

		try
		{
			PreparedStatement stmt = con.prepareStatement(query);
			ResultSet rs = stmt.executeQuery();

			while (rs.next())
			{
				Kunde found = new Kunde();
				found.setKDNR(rs.getInt(1));
				String vorname = rs.getString(2);
				found.setVorname(vorname);
				found.setNachname(rs.getString(3));
				found.setGeschlecht(rs.getString(4));
				found.setBonuspunkte(rs.getInt(5));

				all.add(found);
			}
		} catch (SQLException e)
		{
			e.printStackTrace();
		}
		return all;
	}

	// Issue 5
	public int insertKunde(Kunde nK)
	{
		String insertString = "INSERT INTO Kunden(Vorname, Nachname, Geschlecht, Bonuspunkte) VALUES (?,?,?,?) ";
		int newIdentity = 0;
		int affectedRows = 0;

		try
		{
			PreparedStatement stmtInsert = con.prepareStatement(insertString);

			stmtInsert.setString(1, nK.getVorname());
			stmtInsert.setString(2, nK.getNachname());
			stmtInsert.setString(3, nK.getGeschlecht());
			stmtInsert.setDouble(4, nK.getBonuspunkte());

			affectedRows = stmtInsert.executeUpdate();

			String newIdentityString = "SELECT @@identity ";

			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery(newIdentityString);

			rs.next();

			newIdentity = rs.getInt(1);

		}

		catch (SQLException e)
		{
			//
			e.printStackTrace();
		}
		return newIdentity;
	}

	// Issue 5a
	public void updateKunde(Kunde kunde)
	{
		int affectedRows = 0;
		// 1a - update Statement
		String updateString = "";
		updateString = "UPDATE Kunden SET ";
		updateString += " Vorname = ? ,";
		updateString += " Nachname = ? ,";
		updateString += " Geschlecht = ? ,";
		updateString += " Bonuspunkte = ? ";
		updateString += " WHERE KDNR= ?";

		try
		{
			PreparedStatement stmtUpdate = con.prepareStatement(updateString);

			stmtUpdate.setString(1, kunde.getVorname());
			stmtUpdate.setString(2, kunde.getNachname());
			stmtUpdate.setString(3, kunde.getGeschlecht());
			stmtUpdate.setInt(4, kunde.getBonuspunkte());
			stmtUpdate.setInt(5, kunde.getKDNR());

			affectedRows = stmtUpdate.executeUpdate();

			String newIdentityString = "SELECT @@identity ";

			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery(newIdentityString);

			rs.next();

			int newIdentity = rs.getInt(1);
		}

		catch (SQLException e)
		{
			e.printStackTrace();
		}
	}

	public Kunde getKundeMitDenMeistenBonusPunkten()
	{
		return null;
	}

	public ArrayList<Kunde> getWeiblicheKunden()

	{
		return null;

	}

	public int insertRechnung(Rechnungen r, Kunde k)
	{
		return 0;
	}

	public static void loescheAlleRechnungenUndDannDenKunden(Kunde k)
	{

	}

	public void demoTransaktion()
	{

		String updateString1 = "Update Kunden SET Bonuspunkte = Bonuspunkte + 10 WHERE kdnr = 1";
		String updateString2 = "Update Kunden SET Bonuspunkte = Bonuspunkte + 10 WHERE kdnr = 2";

		try
		{
			// führt nicht sofort zu einem Speichern
			con.setAutoCommit(false);
			PreparedStatement stmtUpdate1 = con.prepareStatement(updateString1);
			int affected1 = stmtUpdate1.executeUpdate();

			PreparedStatement stmtUpdate2 = con.prepareStatement(updateString2);
			int affected2 = stmtUpdate1.executeUpdate();

			// wenn beide passen dann commit
			if (affected1 == 1 && affected2 == 1)
			{
				con.commit();
			}
			if (affected1 == 0 && affected2 == 0)
			{
				con.rollback();
			}

		} catch (SQLException e)
		{
			e.printStackTrace();
		}
	}
}
