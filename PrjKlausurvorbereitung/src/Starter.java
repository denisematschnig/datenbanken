import java.util.*;

public class Starter
{

	public static void main(String[] args)
	{
		DBHelper bee = new DBHelper();
		bee.init();

		System.out.println();

		// Issue 4
		System.out.println("Aufgabe 4 - get Kunde:");
		Kunde k = bee.getKunde(3);
		System.out.printf("Vorname: %s\n", k.getVorname());
		System.out.println();

		// Issue 4a
		System.out.println("Aufgabe 4a - getAlleKunden:");
		System.out.printf("%s ", bee.getAlleKunden());
		System.out.printf("\n\n");

		// //Issue 5
		// System.out.println("Aufgabe 5 - insertKunde:");
		// Kunde nk = new Kunde();
		// nk.setVorname("Andi");
		// nk.setNachname("Hubeer");
		// int newIdentity = bee.insertKunde(nk);
		// System.out.printf("Neuer Autowert %d", newIdentity);
		// System.out.println();

//		Kunde y = new Kunde();
//		y.setVorname("Andi");
//		bee.updateKunde(y);
//		System.out.printf("%s ", bee.updateKunde(y));

		//
		// Kunde meistenPunkte = bee.getKundeMitDenMeistenBonusPunkten();
		// System.out.printf("Bonuspunkte %s", meistenPunkte.getNachname());
		//
		// System.out.println();
		//
		// List<Kunde> frauen = bee.getWeiblicheKunden();
		// for (Kunde kunde : frauen)
		// {
		// System.out.println(kunde.getVorname());
		// }
		//
		// System.out.println();
		//
		bee.close();
	}

}
