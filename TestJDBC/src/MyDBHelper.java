import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;

public class MyDBHelper
{

	private Connection con = null;

	public void init()
		{

			try
			{
				// 1a. Jar-Files referenzieren - Java Build Path
				// 1b. Load Access Driver
				Class.forName("net.ucanaccess.jdbc.UcanaccessDriver");
			} catch (ClassNotFoundException e)
			{

				e.printStackTrace();
			}

			try
			{

				// 2. get Connection to database
				con = DriverManager.getConnection(
						"jdbc:ucanaccess://" + "E:\\Datenbanken\\030518\\DBP\\" + "Urlaubsverwaltung.accdb");

				System.out.println("Super - hat funktioniert :-) ");

			} catch (SQLException e)
			{
				e.printStackTrace();
			}

		}

	public void close()
		{
			if (con != null)
				try
				{
					con.close();
				} catch (SQLException e)
				{
					e.printStackTrace();
					System.out.println("Fehler beim Schliessen der Verbindung");
				}
		}

	public Student getStudent(int studentId)
		{
			String query = "SELECT StudentId, Vorname, Nachname, Credits, Inskribiert, Lieblingsfarbe "
					+ "FROM Student WHERE studentId=? ";
			Student foundStudent = new Student();
			try
			{
				PreparedStatement stmt = con.prepareStatement(query);
				stmt.setInt(1, studentId);
				ResultSet rs = stmt.executeQuery();
				if (rs.next())
				{
					int id = rs.getInt(1);
					foundStudent.setStudentId(id);
					String vorname = rs.getString(2);
					foundStudent.setVorname(vorname);
					foundStudent.setNachname(rs.getString(3));
					foundStudent.setCredits(rs.getDouble(4));
					foundStudent.setInskribiert(rs.getBoolean(5));
					foundStudent.setLieblingsfarbe(rs.getString(6));
					foundStudent.setUrlaubswuensche(getUrlaubswuensche(studentId));

				}
			} catch (SQLException e)
			{
				e.printStackTrace();
			}

			return foundStudent;
		}

	public ArrayList<Urlaubswunsch> getUrlaubswuensche(int studentId)
		{

			ArrayList<Urlaubswunsch> wishes = new ArrayList<Urlaubswunsch>();
			String query = "SELECT Ort, Preis ";
			query += " FROM Urlaubswunsch WHERE studentId=?  ";
			Student s = new Student();

			try
			{
				PreparedStatement stmt = con.prepareStatement(query);
				stmt.setInt(1, studentId);
				ResultSet rs = stmt.executeQuery();
				while (rs.next())
				{

					Urlaubswunsch u = new Urlaubswunsch();
					u.setDestination(rs.getString(1));
					u.setPreis(rs.getDouble(2));
					wishes.add(u);
				}

				s.setUrlaubswuensche(wishes);
			} catch (SQLException e)
			{
				e.printStackTrace();
			}
			return wishes;

		}

	public void printStudentAndUrlaube(int studtId, double biggerAs)
		{
			String query = "SELECT Vorname FROM Student WHERE studentId=? ";
			try
			{
				PreparedStatement stmt = con.prepareStatement(query);
				stmt.setInt(1, studtId);
				ResultSet rs = stmt.executeQuery();
				if (rs.next())
				{
					String vorname = rs.getString(1);
					System.out.printf("\nStudent %s\n", vorname);

					String queryU = "SELECT preis, Ort from Urlaubswunsch where preis>? and studentId=?";

					PreparedStatement stmtU = con.prepareStatement(queryU);
					stmtU.setDouble(1, biggerAs);
					stmtU.setInt(2, studtId);
					ResultSet rsU = stmtU.executeQuery();
					int counter = 0;
					while (rsU.next())
					{
						counter++;
						double preis = rsU.getDouble(1);
						String ort = rsU.getString(2);

						System.out.printf("Preis %.2f Ort %s \n", preis, ort);
					}
					if (counter == 0)
					{
						System.out.println("Dieser Student hat keine Urlaubswüsnche ");
					}
				} else
				{
					System.out.printf("Student %d nicht gefunden ", studtId);
				}
			} catch (SQLException e)
			{
				e.printStackTrace();
			}

		}

	public int updateStudent(int studID, double newC)
		{
			int affectedRows = 0;
			// 1a - update Statement
			String update = "";
			update = "UPDATE Student SET ";
			update += " Credits = ? ";
			update += " WHERE StudentId=?";

			try
			{
				// 1b Prepared Statement
				PreparedStatement stmt = con.prepareStatement(update);

				// 1c - Parameter setzen
				stmt.setDouble(1, newC);
				stmt.setInt(2, studID);

				affectedRows = stmt.executeUpdate();

			} catch (SQLException e)
			{
				//
				e.printStackTrace();
			}
			return affectedRows;

		}

	public int deleteStudent(int studID)
		{
			int affectedRows = 0;

			String delete = "DELETE FROM STUDENT WHERE StudentId=?";

			try
			{
				PreparedStatement stmt = con.prepareStatement(delete);

				stmt.setInt(1, studID);

				affectedRows = stmt.executeUpdate();

			}

			catch (SQLException e)
			{
				e.printStackTrace();
			}
			return affectedRows;

		}

	// helper.updateStudentWhereFirstnameLike("%a%", 45);
	// helper.updateStudentWhereFirstnameLike("a%", 45);
	// helper.updateStudentWhereFirstnameLike("%a", 45);
	public int updateStudentWhereFirstnameLike(String firstnameContainsLetter, double newCredit)
		{

			int affectedRows = 0;
			// 1a - update Statement
			String updateString = "";
			updateString = "UPDATE Student SET ";
			updateString += " Credits = credits + ? ";
			updateString += " WHERE vorname like ?";

			try
			{
				// 1b Prepared Statement
				PreparedStatement stmtUpdate = con.prepareStatement(updateString);

				// 1c - Parameter setzen
				stmtUpdate.setDouble(1, newCredit);
				stmtUpdate.setString(2, firstnameContainsLetter);

				affectedRows = stmtUpdate.executeUpdate();

			} catch (SQLException e)
			{
				//
				e.printStackTrace();
			}
			return affectedRows;

		}

	public int addNewStudent(Student newStudent)
		{

			int affectedRows = 0;
			// 1a - update Statement
			String insertString = "";
			insertString = "INSERT INTO Student(Vorname, Credits) ";
			insertString += " VALUES(?,?) ";

			int newIdentity = 0;
			try
			{
				// 1b Prepared Statement
				PreparedStatement stmtInsert = con.prepareStatement(insertString);

				// 1c - Parameter setzen
				stmtInsert.setString(1, newStudent.getVorname());
				stmtInsert.setDouble(2, newStudent.getCredits());

				affectedRows = stmtInsert.executeUpdate();

				String newIdentityString = "SELECT @@identity ";

				Statement stmt = con.createStatement();
				ResultSet rs = stmt.executeQuery(newIdentityString);

				rs.next();

				newIdentity = rs.getInt(1);

			} catch (SQLException e)
			{
				//
				e.printStackTrace();
			}
			return newIdentity;

		}

	// 1a. neue Klasse Urlaub
	// 1b. Student-Klasse erweitern um eine "Liste von Urlauben"
	// 2a. Zwei Abfragen - 1. Details zu Studierenden, 2. Details zu Urlaubswünschen
	// 2b. Eine Abfrage mit JOIN zwischen Student und Urlaubwunsch
	public Student getStudentWithUrlaubsDetails(int studentId)
		{
			/*
			 * Student s =new Student();
			 * 
			 * //select * from student where studentid=1 s.setVorname("Josefa");
			 * s.setCredits(250);
			 * 
			 * //select * from urlaubswuensche where studentid=1 ArrayList<Urlaubswunsch>
			 * urlaubswuensche=new ArrayList<Urlaubswunsch>(); Urlaubswunsch u1 =new
			 * Urlaubswunsch(); Urlaubswunsch u2 =new Urlaubswunsch();
			 * urlaubswuensche.add(u1); urlaubswuensche.add(u2);
			 * 
			 * s.setUrlaubswuensche(urlaubswuensche);
			 * 
			 * //select s.*, u.* from student s join urlaubwunsch u on
			 * u.StudentId=s.studentid
			 * 
			 * return s;
			 */

			String query = "SELECT StudentId, Vorname, Nachname, Credits, Inskribiert, Lieblingsfarbe, Ort, Preis ";
			query += " FROM Student JOIN Urlaubswunsch on Student.StudentID=Urlaubswunsch.StudentId WHERE studentId=?  ";
			Student s = new Student();
			ArrayList<Urlaubswunsch> urlaubswuensche = new ArrayList<Urlaubswunsch>();

			try
			{
				PreparedStatement stmt = con.prepareStatement(query);
				stmt.setInt(1, studentId);
				ResultSet rs = stmt.executeQuery();
				while (rs.next())
				{
					s.setStudentId(rs.getInt(1));
					s.setVorname(rs.getString(2));
					s.setNachname(rs.getString(3));
					s.setCredits(rs.getDouble(4));
					s.setInskribiert(rs.getBoolean(5));
					s.setLieblingsfarbe(rs.getString(6));

					Urlaubswunsch u = new Urlaubswunsch();
					u.setDestination(rs.getString(7));
					u.setPreis(rs.getDouble(8));
					urlaubswuensche.add(u);
				}

				s.setUrlaubswuensche(urlaubswuensche);
			} catch (SQLException e)
			{
				e.printStackTrace();
			}
			return s;

		}

	public HashMap<String, String> getColumnNamesForTableStudentFromMetadata()
		{
			HashMap<String, String> metaData = new HashMap<String, String>();
			try
			{
				ResultSet rs = con.prepareStatement("SELECT StudentId,Vorname, Inskribiert FROM Student ")
						.executeQuery();

				ResultSetMetaData meta = rs.getMetaData();
				for (int i = 1; i <= meta.getColumnCount(); i++)
				{
					metaData.put(meta.getColumnName(i), meta.getColumnTypeName(i));
				}
			} catch (SQLException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return metaData;
		}

	// 16.11 Metadaten
	public void printColumnNamesForTableStudentFromMetadata()
		{
			try
			{
				ResultSet rs = con.prepareStatement("SELECT StudentId,Vorname, Inskribiert FROM Student ")
						.executeQuery();

				ResultSetMetaData meta = rs.getMetaData();

				for (int i = 1; i <= meta.getColumnCount(); i++)
				{
					System.out.printf("\n\t %-20s %-20s%n", meta.getColumnName(i), meta.getColumnTypeName(i));
				}
			} catch (SQLException e)
			{
				e.printStackTrace();
			}

		}

	public void testTransaction(Student newStudent)
		{
			try
			{
				int affectedRows = 0;
				con.setAutoCommit(false);
				// 1a - update Statement
				String insertString = "";
				insertString = "INSERT INTO Student(Vorname, Credits) ";
				insertString += " VALUES(?,?) ";

				int newIdentity = 0;

				// 1b Prepared Statement
				PreparedStatement stmtInsert = con.prepareStatement(insertString);

				// 1c - Parameter setzen
				stmtInsert.setString(1, newStudent.getVorname());
				stmtInsert.setDouble(2, newStudent.getCredits());

				affectedRows = stmtInsert.executeUpdate();

				String newIdentityString = "SELECT @@identity ";

				Statement stmt = con.createStatement();
				ResultSet rs = stmt.executeQuery(newIdentityString);

				rs.next();

				newIdentity = rs.getInt(1);
				con.commit();
			} catch (SQLException e)
			{
				//
				e.printStackTrace();
			}

		}

}
