import java.util.ArrayList;
import java.util.HashMap;

public class MyStarter
{

	public static void main(String[] args)
	{
		// System.out.println("Hello Wordl");

		MyDBHelper helper = new MyDBHelper();
		helper.init(); // Driver load, Con open

		Student student1 = helper.getStudent(1);

		System.out.printf("\nStudent 1: Vorname %s Credits %f", student1.getVorname(), student1.getCredits());

		// 1a. neue Klasse Urlaub
		// 1b. Student-Klasse erweitern um eine "Liste von Urlauben"
		// 2a. Zwei Abfragen - 1. Details zu Studierenden, 2. Details zu Urlaubswünschen
		// 2b. Eine Abfrage mit JOIN zwischen Student und Urlaubwunsch
		Student studentDetails = null;
		studentDetails = helper.getStudentWithUrlaubsDetails(1); // MIT JOIN

		studentDetails = helper.getStudent(1); // Mit Hilfsmethode - geturlaubswuensche

		System.out.printf("\n\nDetails zu %d , Vorname %s", studentDetails.getStudentId(), studentDetails.getVorname());

		System.out.printf("\nUrlaubsdetails");

		studentDetails.getUrlaubswuensche()
				.forEach((u) -> System.out.printf("\nOrt: %s Preis %f", u.getDestination(), u.getPreis()));

		System.out.println("Alle Studierenden");
		helper.printStudentAndUrlaube(1, 0);

		int result = helper.updateStudent(1, 30);

		if (result == 0)
		{
			System.out.println("\nKein Student vom Update betroffen");
		} else
		{

			System.out.printf("\n%d Studierende betroffen ", result);
		}

		result = helper.updateStudentWhereFirstnameLike("%a%", 45);

		if (result == 0)
		{
			System.out.println("\nKein Student vom Update betroffen");
		} else
		{

			System.out.printf("\n%d Studierende betroffen ", result);
		}

		// result = helper.deleteStudent(1);
		// System.out.printf("\n%d Studierende vom Delete betroffen ", result);

		// Student newStudent=new Student();
		// newStudent.setVorname("Kevin");
		// newStudent.setCredits(7);
		// result = helper.addNewStudent(newStudent);
		// System.out.printf("\nStudent hinzugefügt - neue ID: %d ",result);

		helper.printColumnNamesForTableStudentFromMetadata();

		HashMap<String, String> resultMap = helper.getColumnNamesForTableStudentFromMetadata();

		// System.out.print(resultMap);

		resultMap.forEach(// Lambda
				(x, y) -> System.out.printf("\n%-20s %-20s", x, y));

		// newStudent.setVorname("Ursula");
		// helper.testTransaction(newStudent);

		helper.close(); // Connection Close

	}

}
