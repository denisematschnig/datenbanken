import java.util.ArrayList;

public class Student {

	public String getVorname() {
		return Vorname;
	}

	public void setVorname(String vorname) {
		Vorname = vorname;
	}

	public double getCredits() {
		return Credits;
	}

	public void setCredits(double credits) {
		Credits = credits;
	}

	private String Vorname;
	
	
	private String Nachname;	
	public String getNachname() {
		return Nachname;
	}

	public void setNachname(String nachname) {
		Nachname = nachname;
	}

	public boolean isInskribiert() {
		return Inskribiert;
	}

	public void setInskribiert(boolean inskribiert) {
		Inskribiert = inskribiert;
	}

	public String getLieblingsfarbe() {
		return Lieblingsfarbe;
	}

	public void setLieblingsfarbe(String lieblingsfarbe) {
		Lieblingsfarbe = lieblingsfarbe;
	}

	private boolean Inskribiert;
	private String Lieblingsfarbe;
	
	private double Credits;
	
	private int StudentId;
	public int getStudentId() {
		return StudentId;
	}

	public void setStudentId(int studentId) {
		StudentId = studentId;
	}
	
	private ArrayList<Urlaubswunsch> Urlaubswuensche;
	public ArrayList<Urlaubswunsch> getUrlaubswuensche() {
		return Urlaubswuensche;
	}

	public void setUrlaubswuensche(ArrayList<Urlaubswunsch> urlaubswuensche) {
		Urlaubswuensche = urlaubswuensche;
	}
	
	
}
