import java.util.List;

public class MyStarter {

	public static void main(String[] args) {
		MyDBHelper helper=new MyDBHelper();
		helper.init();
		
		helper.demoTransaktion();
		
		Kunde kunde1 = helper.getKunde(1);
		System.out.println(kunde1);
		
		
		List<Kunde> alleMeineKunden = helper.getAlleKunden();
		System.out.println("\nAlle Kunden");
		System.out.println(alleMeineKunden);
		
		Kunde neuerKunde = new Kunde();
		neuerKunde.setVorname("Tester");
		neuerKunde.setNachname("Josef");
		neuerKunde.setGeschlecht("M");
		neuerKunde.setBonsupunkte(500);
		
	//	neuerKunde.setKDNR(helper.insertKunde(neuerKunde));

		
		
		Kunde kAendern=new Kunde();
		/*kAendern.setVorname("Karl");
		kAendern.setKDNR(1);
		helper.updateKunde(kAendern);*/
		
		kAendern=helper.getKunde(1);
		kAendern.setVorname("Johann");
		helper.updateKunde(kAendern);
		
		Rechnungen neueRechnung=new Rechnungen();
		neueRechnung.setGesamtbetrag(227);
		neueRechnung.setDatum("04.05.2018");
		
		//neueRechnung.setReNr(helper.insertRechnung(neueRechnung, kAendern));
		
		Rechnungen rechnungAendern=new Rechnungen();
		rechnungAendern.setReNr(1);
		rechnungAendern.setKDNR(2); //Vorischt g�ltige KDNR
		rechnungAendern.setDatum("05.05.2018");
		rechnungAendern.setGesamtbetrag(120);
		
		helper.updateRechnung(rechnungAendern);
		
		/*
		System.out.println("\nAufgabe 4 - public Kunde getKunde(int kdnr)\n");
		
		Kunde kunde1 = helper.getKunde(1);
		System.out.println(kunde1);
		
		kunde1.setVorname("Johann neu");
		kunde1.setBonsupunkte(200);
		
		helper.updateKunde(kunde1);
		
		
		Kunde kNeu=new Kunde();
		kNeu.setVorname("Margret");
		kNeu.setNachname("Test");
		//helper.insertKunde(kNeu);
		*/
		helper.close();

	}

}

