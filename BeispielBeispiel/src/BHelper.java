import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.*;

public class BHelper
{
	private Connection con = null;

	// initialise Connection
	public void init()
	{

		try
		{
			Class.forName("net.ucanaccess.jdbc.UcanaccessDriver");
		}

		catch (ClassNotFoundException e)
		{

			e.printStackTrace();
		}

		try
		{
			con = DriverManager.getConnection(
					"jdbc:ucanaccess://" + "E:\\Datenbanken\\BeispielBeispiel\\" + "BeispielDatenbank.accdb");

			System.out.println("Super - hat funktioniert :-) ");

		} catch (SQLException e)
		{
			e.printStackTrace();
		}
	}

	// select all in array
	public List<Studenten> getAll()
	{
		ArrayList<Studenten> students = new ArrayList<Studenten>();
		String query = "SELECT StudentID, Vorname, Nachname, Lieblingsgenre, Bonuspunkte FROM Studenten";
		try
		{
			PreparedStatement stmt = con.prepareStatement(query);
			ResultSet rs = stmt.executeQuery();
			while (rs.next())
			{
				students.add(getStudent(rs.getInt(1)));
			}
		}

		catch (SQLException e)
		{
			e.printStackTrace();
		}

		return students;
	}

	// Select with ID
	public Studenten getStudent(int studID)
	{
		String query = "SELECT StudentID, Vorname, Nachname, Lieblingsgenre, Bonuspunkte FROM Studenten WHERE StudentID = ? ";
		Studenten n = new Studenten();

		try
		{
			PreparedStatement stmt = con.prepareStatement(query);
			stmt.setInt(1, studID);
			ResultSet rs = stmt.executeQuery();

			if (rs.next())
			{
				int id = rs.getInt(1);
				n.setStudID(id);
				n.setVorname(rs.getString(2));
				n.setNachname(rs.getString(3));
				n.setLieblingsgenre(rs.getInt(4));
				n.setBonuspunkte(rs.getInt(5));
			}
		} catch (SQLException e)
		{
			e.printStackTrace();
		}
		return n;

	}

	public Musikgenre getGenre(int genreID)
	{
		String query = "SELECT GenreID, Genre, Artist FROM Musikrichtung WHERE GenreID = ? ";
		Musikgenre n = new Musikgenre();

		try
		{
			PreparedStatement stmt = con.prepareStatement(query);
			stmt.setInt(1, genreID);
			ResultSet rs = stmt.executeQuery();

			if (rs.next())
			{
				int id = rs.getInt(1);
				n.setGenreID(id);
				n.setGenre(rs.getString(2));
				n.setArtist(rs.getString(3));
			}
		}

		catch (SQLException e)
		{
			e.printStackTrace();
		}
		return n;
	}

	// insert new entry
	public int insertStudent(Studenten newS)
	{
		int affectedRows = 0;
		String insert = "INSERT INTO Studenten(Vorname, Nachname, Lieblingsgenre, Bonuspunkte) VALUES (?,?,?,?)";
		int newID = 0;
		try
		{
			con.setAutoCommit(true);
			PreparedStatement stmt = con.prepareStatement(insert);

			stmt.setString(1, newS.getVorname());
			stmt.setString(2, newS.getNachname());
			stmt.setInt(3, newS.getLieblingsgenre());
			stmt.setInt(4, newS.getBonuspunkte());

			affectedRows = stmt.executeUpdate();

			String newIDS = "SELECT @@identity";
			Statement stmt2 = con.createStatement();
			ResultSet rs = stmt2.executeQuery(newIDS);
			rs.next();
			newID = rs.getInt(1);
		}

		catch (SQLException e)
		{
			e.printStackTrace();
		}
		return newID;
	}

	// end connection
	public void close()
	{
		if (con != null)
			try
			{
				con.close();
			} catch (SQLException e)
			{
				e.printStackTrace();
				System.out.println("Fehler beim Schliessen der Verbindung");
			}
	}

	// update
	public void updateStudent(Studenten stud)
	{
		int affectedRows = 0;
		String update = "";
		update += "UPDATE Studenten SET ";
		update += "Vorname = ?, Nachname = ?, Lieblingsgenre = ?, Bonuspunkte = ?";
		update += "WHERE StudentID = ?";
		try
		{
			PreparedStatement stmt = con.prepareStatement(update);
			stmt.setString(1, stud.getVorname());
			stmt.setString(2, stud.getNachname());
			stmt.setInt(3, stud.getLieblingsgenre());
			stmt.setInt(4, stud.getBonuspunkte());
			stmt.setInt(5, stud.getStudID());

			affectedRows = stmt.executeUpdate();

		}

		catch (SQLException e)
		{
			e.printStackTrace();
		}
	}

	// update with parameter
	public int updateCredit(int studID, int newC)
	{
		int affectedRows = 0;
		// 1a - update Statement
		String update = "";
		update = "UPDATE Studenten SET Bonuspunkte = ? WHERE StudentId=?";

		try
		{
			// 1b Prepared Statement
			PreparedStatement stmt = con.prepareStatement(update);

			// 1c - Parameter setzen
			stmt.setInt(1, newC);
			stmt.setInt(2, studID);

			affectedRows = stmt.executeUpdate();

		} catch (SQLException e)
		{
			//
			e.printStackTrace();
		}

		return affectedRows;

	}

	// change existing value of data
	public void transaktion()
	{
		String update1 = "UPDATE Studenten SET bonuspunkte = bonuspunkte + 10 where StudentID=5";
		String update2 = "UPDATE Studenten SET bonuspunkte = bonuspunkte - 10 where StudentID=8";

		try
		{
			con.setAutoCommit(true);
			PreparedStatement stmt1 = con.prepareStatement(update1);
			int affected1 = stmt1.executeUpdate();

			PreparedStatement stmt2 = con.prepareStatement(update2);
			int affected2 = stmt2.executeUpdate();
		}

		catch (SQLException e)
		{
			e.printStackTrace();
		}
	}

	// parametered transaction
	public void paraTransaktion(Studenten newS)
	{
		try
		{
			int affectedRows = 0;
			con.setAutoCommit(false);
			// 1a - update Statement
			String insert = "";
			insert = "INSERT INTO Studenten(Vorname, Bonuspunkte) ";
			insert += " VALUES(?,?) ";

			int newIdentity = 0;

			// 1b Prepared Statement
			PreparedStatement stmtInsert = con.prepareStatement(insert);

			// 1c - Parameter setzen
			stmtInsert.setString(1, newS.getVorname());
			stmtInsert.setDouble(2, newS.getBonuspunkte());

			affectedRows = stmtInsert.executeUpdate();

			String newIdentityString = "SELECT @@identity ";

			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery(newIdentityString);

			rs.next();

			newIdentity = rs.getInt(1);
			con.commit();
		}

		catch (SQLException e)
		{
			e.printStackTrace();
		}

	}

	// inner join
	public Studenten getStudentGenre(int studID)
	{
		String query = "SELECT StudentID, Vorname, Nachname, Bonuspunkte, GenreID, Genre, Artist "
				+ "FROM Studenten JOIN Musikrichtung on Musikrichtung.GenreID=Studenten.Lieblingsgenre WHERE StudentID=?";

		Studenten s = new Studenten();
		ArrayList<Musikgenre> geschmack = new ArrayList<Musikgenre>();

		try
		{
			PreparedStatement stmt = con.prepareStatement(query);
			stmt.setInt(1, studID);
			ResultSet rs = stmt.executeQuery();
			while (rs.next())
			{
				s.setStudID(rs.getInt(1));
				s.setVorname(rs.getString(2));
				s.setNachname(rs.getString(3));
				s.setBonuspunkte(rs.getInt(4));

				Musikgenre m = new Musikgenre();
				m.setGenreID(rs.getInt(5));
				m.setGenre(rs.getString(6));
				m.setArtist(rs.getString(7));

				geschmack.add(m);

			}
			s.setGenre(geschmack);

		} catch (SQLException e)
		{
			e.printStackTrace();
		}
		return s;
	}

	// update like
	public int updateLike(String letter, int newC)
	{

		int affectedRows = 0;
		// 1a - update Statement
		String updateString = "";
		updateString = "UPDATE Studenten SET Bonuspunkte = Bonuspunkte + ? ";
		updateString += " WHERE Vorname like ?";

		try
		{
			// 1b Prepared Statement
			PreparedStatement stmtUpdate = con.prepareStatement(updateString);

			// 1c - Parameter setzen
			stmtUpdate.setInt(1, newC);
			stmtUpdate.setString(2, letter);

			affectedRows = stmtUpdate.executeUpdate();

		} catch (SQLException e)
		{
			//
			e.printStackTrace();
		}

		return affectedRows;

	}

	// print MetaDaten
	public void printMetaData()
	{
		try
		{
			ResultSet rs = con.prepareStatement("SELECT StudentId,Vorname, Nachname FROM Studenten ").executeQuery();

			ResultSetMetaData meta = rs.getMetaData();

			for (int i = 1; i <= meta.getColumnCount(); i++)
			{
				System.out.printf("\n\t %-20s %-20s%n", meta.getColumnName(i), meta.getColumnTypeName(i));
			}
		} catch (SQLException e)
		{
			e.printStackTrace();
		}
	}

	// get MetaDaten in HashMap // print MetaDaten in HahMap
	public HashMap<String, String> getMetaData()
	{
		HashMap<String, String> metaData = new HashMap<String, String>();
		try
		{
			ResultSet rs = con.prepareStatement("SELECT StudentId,Vorname, Nachname FROM Studenten ").executeQuery();

			ResultSetMetaData meta = rs.getMetaData();
			for (int i = 1; i <= meta.getColumnCount(); i++)
			{
				metaData.put(meta.getColumnName(i), meta.getColumnTypeName(i));
			}
		} catch (SQLException e)
		{
			e.printStackTrace();
		}
		return metaData;
	}

	// delete
	public int deleteStudent(int studID)
	{
		int affectedRows = 0;
		int newID = 0;
		String delete = "DELETE FROM Studenten WHERE StudentID=?";

		try
		{
			PreparedStatement stmt = con.prepareStatement(delete);
			stmt.setInt(1, studID);
			affectedRows = stmt.executeUpdate();

			String newIDS = "SELECT @@identity";
			Statement stmt2 = con.createStatement();
			ResultSet rs = stmt2.executeQuery(newIDS);
			rs.next();
			newID = rs.getInt(1);

		}

		catch (SQLException e)
		{
			e.printStackTrace();
		}
		return newID;

	}

	// get students with genre
	public ArrayList<Studenten> getStudentsGenre(int genre)
	{
		ArrayList<Studenten> musikgeschmack = new ArrayList<Studenten>();
		String query = "SELECT StudentID, Vorname, Nachname FROM Studenten WHERE Lieblingsgenre=genre";

		try
		{
			PreparedStatement stmt = con.prepareStatement(query);

			ResultSet rs = stmt.executeQuery();
			while (rs.next())
			{
				musikgeschmack.add(getStudent(rs.getInt(1)));

			}
		} catch (SQLException e)
		{
			e.printStackTrace();
		}

		return musikgeschmack;
	}
}
