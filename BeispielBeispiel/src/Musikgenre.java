
public class Musikgenre
{
	private int GenreID;
	private String Genre;
	private String Artist;

	public int getGenreID()
	{
		return GenreID;
	}

	public void setGenreID(int genreID)
	{
		GenreID = genreID;
	}

	public String getGenre()
	{
		return Genre;
	}

	public void setGenre(String genre)
	{
		Genre = genre;
	}

	public String getArtist()
	{
		return Artist;
	}

	public void setArtist(String artist)
	{
		Artist = artist;
	}

	@Override
	public String toString()
	{
		return String.format("(%d - %s - %s)\n", GenreID, Genre, Artist);
		// return GenreID + Genre + Artist + " -\n";
	}
}
